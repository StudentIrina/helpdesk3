package com.imedvedeva.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;
    private final DtoConverter converter;

    public TicketController(TicketService ticketService, DtoConverter converter) {
        this.ticketService = ticketService;
        this.converter = converter;
    }

    @GetMapping("/all")
    ResponseEntity<List<TicketDto>> getAllTickets() {
        List<TicketDto> ticketDtoList = ticketService
                .getAll()
                .stream()
                .map(converter::convertToDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(ticketDtoList);
    }

    @GetMapping("/my")
    ResponseEntity<List<TicketDto>> getMyTickets() {
        List<TicketDto> ticketDtoList = ticketService
                .getMyTickets()
                .stream()
                .map(converter::convertToDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(ticketDtoList);
    }

    @PostMapping
    ResponseEntity<Void> createNewTicket(@RequestBody TicketDto ticketDto) {
        System.out.println(ticketDto.toString());
        ticketService.saveNewTicket(ticketDto);
        URI location = fromCurrentRequest().buildAndExpand(ticketDto.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

}