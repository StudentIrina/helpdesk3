package com.imedvedeva.dto;

import com.imedvedeva.entity.User;

import java.util.List;

public interface UserDTO {
    List<User> getUsers();

    void saveUser(User user);

    void deleteUser(Long id);

    User getUser(Long id);
}

