package com.imedvedeva.dto;

import com.imedvedeva.entity.Ticket;


import java.util.List;

public interface TicketsDTO {
    List<Ticket> getTickets();

    void saveTicket(Ticket ticket);

    void deleteTicket(Long id);

   Ticket getUser(Long id);
}
