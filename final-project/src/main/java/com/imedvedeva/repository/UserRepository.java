package com.imedvedeva.repository;

import com.imedvedeva.entity.User;

public interface UserRepository {

    User findUserByLogin(String login);

    void save(User user);
}
