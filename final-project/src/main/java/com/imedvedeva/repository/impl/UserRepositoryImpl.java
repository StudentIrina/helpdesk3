package com.imedvedeva.repository.impl;

import com.imedvedeva.entity.enums.Role;
import com.imedvedeva.entity.User;
import com.imedvedeva.repository.UserRepository;
import org.hibernate.tool.hbm2ddl.ConnectionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class UserRepositoryImpl implements UserRepository{
    private static final Logger LOG = LoggerFactory.getLogger(UserRepositoryImpl.class);
    private static final String SQL_SELECT_BY_LOGIN = "select * from users as u where u.login= ?";
    private static final String SQL_INSERT = "insert into users (name,login,password,role) values(?,?,?,?)";


    @Override
    public User findUserByLogin(String login) {
        try (Connection connection = ConnectionHelper.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_LOGIN)) {
                preparedStatement.setString(1, login);
                final ResultSet resultSet = preparedStatement.executeQuery();
                return getUser(resultSet);
            }
        } catch (SQLException e) {
            LOG.error("Error while select from Users, login:{[]}", login, e.getMessage());
        }
        return null;
    }

    @Override
    public void save(User user) {
        try (Connection connection = ConnectionHelper.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT)) {
                preparedStatement.setString(1, user.getFirst_name());
                preparedStatement.setString(2, user.getLast_name());
                preparedStatement.setString(3, user.getPassword());
                preparedStatement.setString(4, user.getRole_id().toString());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.error("Error while insert into Users", e.getMessage());
        }

    }

    private User getUser(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            return User.builder()
                    .id(resultSet.getLong("id"))
                    .login(resultSet.getString("login"))
                    .name(resultSet.getString("name"))
                    .password(resultSet.getString("password"))
                    .role(Role.valueOf(resultSet.getString("role")))
                    .build();
        }
        return null;
    }
}


