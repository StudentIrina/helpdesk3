package com.imedvedeva.repository;

import com.imedvedeva.entity.Ticket;
import com.imedvedeva.entity.User;

import java.awt.*;
import java.util.List;

public interface TicketRepository {


    Ticket findTicketByUser(User user);

    void save(Ticket ticket);

    List<Ticket> getAllTickets();
}




