package com.imedvedeva.entity;

import com.imedvedeva.entity.enums.Role;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User implements Serializable {
    Long id;
    String first_name;
    String last_name;
    Role role_id;
    String email;
    String password;

    public User()
    {

    }

    public User(Long id,
                String first_name,
                String last_name,
                Role role_id,
                String email,
                String password) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.role_id = role_id;
        this.email = email;
        this.password = password;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Role getRole_id() {
        return role_id;
    }

    public void setRole_id(Role role_id) {
        this.role_id = role_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId() == user.getId() &&
                getFirst_name().equals(user.getFirst_name()) &&
                getLast_name().equals(user.getLast_name()) &&
                getRole_id() == user.getRole_id() &&
                getEmail().equals(user.getEmail()) &&
                getPassword().equals(user.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirst_name(), getLast_name(), getRole_id(), getEmail(), getPassword());
    }

    @Override
    public String toString() {
        return "Пользователь " +
                " " + id + '\n' +
                " Имя и Фамилия  " + first_name + "  " + last_name + '\n' +
                " Роль  " + role_id + '\n' +
                " email  " + email + '\n' +
                " пароль   " + password + '\n';
    }


}
