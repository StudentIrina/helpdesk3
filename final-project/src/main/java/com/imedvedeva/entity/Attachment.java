package com.imedvedeva.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "attachments")
public class Attachment implements Serializable {
    long id;
    byte blob;
    long ticket_id;
    String name;

    public Attachment() {

    }

    public Attachment(long id, byte blob, long ticket_id, String name) {
        this.id = id;
        this.blob = blob;
        this.ticket_id = ticket_id;
        this.name = name;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte getBlob() {
        return blob;
    }

    public void setBlob(byte blob) {
        this.blob = blob;
    }

    public long getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(long ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Attachment)) return false;
        Attachment that = (Attachment) o;
        return getId() == that.getId() &&
                getBlob() == that.getBlob() &&
                getTicket_id() == that.getTicket_id() &&
                getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBlob(), getTicket_id(), getName());
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "id=" + id +
                ", blob=" + blob +
                ", ticket_id=" + ticket_id +
                ", name='" + name + '\'' +
                '}';
    }
}
