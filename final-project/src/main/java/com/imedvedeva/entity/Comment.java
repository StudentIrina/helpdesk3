package com.imedvedeva.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "comments")
public class Comment implements Serializable {
    long id;
    long user_id;
    String text;
    LocalDate date;
    long ticket_id;

    public Comment(){

    }


    public Comment(long id, long user_id, String text, LocalDate date, long ticket_id) {
        this.id = id;
        this.user_id = user_id;
        this.text = text;
        this.date = date;
        this.ticket_id = ticket_id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public long getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(long ticket_id) {
        this.ticket_id = ticket_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;
        Comment comment = (Comment) o;
        return getId() == comment.getId() &&
                getUser_id() == comment.getUser_id() &&
                getTicket_id() == comment.getTicket_id() &&
                getText().equals(comment.getText()) &&
                getDate().equals(comment.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser_id(), getText(), getDate(), getTicket_id());
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", text='" + text + '\'' +
                ", date=" + date +
                ", ticket_id=" + ticket_id +
                '}';
    }
}
