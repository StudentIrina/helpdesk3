package com.imedvedeva.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "feedbacks")
public class Feedback implements Serializable {

    long id;
    long user_id;
    int rate;
    LocalDate date;
    String text;
    long ticket_id;

    public Feedback(){

    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(long ticket_id) {
        this.ticket_id = ticket_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Feedback)) return false;
        Feedback feedback = (Feedback) o;
        return getId() == feedback.getId() &&
                getUser_id() == feedback.getUser_id() &&
                getRate() == feedback.getRate() &&
                getTicket_id() == feedback.getTicket_id() &&
                getDate().equals(feedback.getDate()) &&
                getText().equals(feedback.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser_id(), getRate(), getDate(), getText(), getTicket_id());
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", rate=" + rate +
                ", date=" + date +
                ", text='" + text + '\'' +
                ", ticket_id=" + ticket_id +
                '}';
    }
}
