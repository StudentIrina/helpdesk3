package com.imedvedeva.entity.enums;

public enum State {DRAFT, NEW, APPROVED, DECLINED, IN_PROGRESS, DONE, CANCELED}
