package com.imedvedeva.entity.enums;

public enum Urgency {CRITICAL, HIGH, AVERAGE, LOW}
