package com.imedvedeva.entity.enums;

public enum Role {EMPLOYEE, MANAGER, ENGINEER}
