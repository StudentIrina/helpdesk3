package com.imedvedeva.entity;

import com.imedvedeva.entity.enums.State;
import com.imedvedeva.entity.enums.Urgency;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "tickets")
public class Ticket implements Serializable {
    long id;
    String name;
    String description;
    String created_on;
    LocalDate desired_resolution_date;
    long assignee_id;
    long owner_id;
    State state_id;
    long category_id;
    Urgency urgency_id;
    long approver_id;

    public Ticket() {

    }

    public Ticket(long id, String name, String description, String created_on, LocalDate desired_resolution_date, long assignee_id, long owner_id, State state_id, long category_id, Urgency urgency_id, long approver_id) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.created_on = created_on;
        this.desired_resolution_date = desired_resolution_date;
        this.assignee_id = assignee_id;
        this.owner_id = owner_id;
        this.state_id = state_id;
        this.category_id = category_id;
        this.urgency_id = urgency_id;
        this.approver_id = approver_id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public LocalDate getDesired_resolution_date() {
        return desired_resolution_date;
    }

    public void setDesired_resolution_date(LocalDate desired_resolution_date) {
        this.desired_resolution_date = desired_resolution_date;
    }

    public long getAssignee_id() {
        return assignee_id;
    }

    public void setAssignee_id(long assignee_id) {
        this.assignee_id = assignee_id;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public State getState_id() {
        return state_id;
    }

    public void setState_id(State state_id) {
        this.state_id = state_id;
    }

    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public Urgency getUrgency_id() {
        return urgency_id;
    }

    public void setUrgency_id(Urgency urgency_id) {
        this.urgency_id = urgency_id;
    }

    public long getApprover_id() {
        return approver_id;
    }

    public void setApprover_id(long approver_id) {
        this.approver_id = approver_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;
        Ticket ticket = (Ticket) o;
        return getId() == ticket.getId() &&
                getAssignee_id() == ticket.getAssignee_id() &&
                getOwner_id() == ticket.getOwner_id() &&
                getCategory_id() == ticket.getCategory_id() &&
                getApprover_id() == ticket.getApprover_id() &&
                getName().equals(ticket.getName()) &&
                getDescription().equals(ticket.getDescription()) &&
                getCreated_on().equals(ticket.getCreated_on()) &&
                getDesired_resolution_date().equals(ticket.getDesired_resolution_date()) &&
                getState_id() == ticket.getState_id() &&
                getUrgency_id() == ticket.getUrgency_id();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getCreated_on(), getDesired_resolution_date(), getAssignee_id(), getOwner_id(), getState_id(), getCategory_id(), getUrgency_id(), getApprover_id());
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", created_on='" + created_on + '\'' +
                ", desired_resolution_date='" + desired_resolution_date + '\'' +
                ", assignee_id=" + assignee_id +
                ", owner_id=" + owner_id +
                ", state_id=" + state_id +
                ", category_id=" + category_id +
                ", urgency_id=" + urgency_id +
                ", approver_id=" + approver_id +
                '}';
    }
}
