package com.imedvedeva.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "histories")
public class History implements Serializable {
    Long id;
    Long ticket_id;
    LocalDate date;
    String action;
    Long user_id;
    String description;

    public History()
    {

    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(Long ticket_id) {
        this.ticket_id = ticket_id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof History)) return false;
        History history = (History) o;
        return getId().equals(history.getId()) &&
                getTicket_id().equals(history.getTicket_id()) &&
                getDate().equals(history.getDate()) &&
                getAction().equals(history.getAction()) &&
                getUser_id().equals(history.getUser_id()) &&
                getDescription().equals(history.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTicket_id(), getDate(), getAction(), getUser_id(), getDescription());
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", ticket_id=" + ticket_id +
                ", date=" + date +
                ", action='" + action + '\'' +
                ", user_id=" + user_id +
                ", description='" + description + '\'' +
                '}';
    }
}
